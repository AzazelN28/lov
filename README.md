# Legends of Valour Web

I remember this game as one of those magical things that only happen to you when
you're an impressionable teenager. My idea with this is to make a remake of the
game with some new features.

## Reverse Engineering

### Places

#### Prisons

P1 Turret Jail
P2 Castle Dungeon
P3 Hireling Prison
P4 The Brig
P5 Twon Gaol

#### Taverns

B1 The Dragons Head
B2 The Trolls Arms
B3 The Hanged Man
B4 The Jug of Ale
B5 The Casino
B6 The Seahorse Tavern
B7 The Mermaids Rest
B8 The Snakes

#### Hostels

H1 The Waifs Rest
H2 Dead Mans Inn
H3 The Flea Pit
H4 The Travellers Inn
H5 The Boardings
H6 The Hermits Repose
H7 The Thespians Tavern
H8 The Seamens Lodgings

#### Shops
